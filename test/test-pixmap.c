/*
** UTTIL: Uniform Transparent/Translucent Imaging Library
** Copyright (c) 1999, Dylan R. Evans/t'Sade
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

/***** Includes *****/
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/shape.h>
#include <Imlib.h>

/********** Main **********/
void draw_window(Display *disp, Window win, ImlibData *id, ImlibImage *im)
{
  /* Variables */
  Pixmap im_pix, im_msk, u_pix, r_pix;
  int dummy, w, h, d;
  Window dw;
  struct timeval start_time;

  /* Start time */
  gettimeofday(&start_time, NULL);

  /* Get the geometry of the window */
  XGetGeometry(disp, win, &dw, &dummy, &dummy, &w, &h, &dummy, &d);

  /*
  ** Deal with the image
  */
  
  /* Render the original 24-bit Image data into a pixmap of size w * h */
  Imlib_render(id, im, w, h);

  /* Extract the Image and mask pixmaps from the Image */
  im_pix = Imlib_move_image(id, im);

  /* The mask will be 0 if the image has no transparency */
  im_msk = Imlib_move_mask(id, im);

  /*
  ** Get the background
  */

  /* Get the background pixmap for this one */
  u_pix = uttil_get_window_desktop_pixmap(disp, win);

  /* 
  ** Combine them and and display them
  */

  /* Create a new pixmap */
  r_pix = XCreatePixmap(disp, win, w, h, d);

  /* Combine them */
  im_pix = uttil_blend_p_p(disp, im_pix, u_pix, r_pix, w, h);

  /* Put the Image pixmap in the background of the window */
  XSetWindowBackgroundPixmap(disp, win, r_pix);

  /* If there was a mask to the image, set the Image's mask to it */
  if(im_msk)
    XShapeCombineMask(disp, win, ShapeBounding, 0, 0, im_msk, ShapeSet);

  /* Sync */
  XClearWindow(disp, win);
  XFreePixmap(disp, r_pix);
  XFreePixmap(disp, im_pix);
  XFreePixmap(disp, im_msk);
  XFreePixmap(disp, u_pix);
  XSync(disp, False);

  /* All done */
  report_time(&start_time);
}

int main(int argc, char **argv)
{
  /* Variables */
  Display *disp;
  ImlibData *id;
  XSetWindowAttributes attr;
  Window win;
  ImlibImage *im;
  Pixmap p,m;
  int w,h;
     
  /* Connect to the default Xserver */
  disp=XOpenDisplay(NULL);

  /* Immediately afterwards Intitialise Imlib */
  id=Imlib_init(disp);

  /* Be nice and tell the user if they don't, to provide a file as an arg */
  if(argc <= 1) {
    im = Imlib_load_image(id, "test.png");
  } else {
    /* Load the image specified as the first argument */
    im=Imlib_load_image(id,argv[1]);
  }

  /* Suck the image's original width and height out of the Image structure */
  w=im->rgb_width;h=im->rgb_height;

  /* Create a Window to display in */
  win = XCreateWindow(disp, DefaultRootWindow(disp), 0, 0, w, h, 0,
		      id->x.depth,
		      InputOutput, id->x.visual, 0, &attr);
  XSelectInput(disp,win,StructureNotifyMask);
  XStoreName(disp, win, "Translucent Pixmap+Desktop Demo");

  /* Display the window */
  draw_window(disp, win, id, im);

  /* Actually display the window */
  XMapWindow(disp,win);

  /* Event loop to handle resizes */   
  for(;;) {
    /* Variables */
    XEvent ev;
    
    /* Sit and wait for an event to happen */ 
    XNextEvent(disp,&ev);
    if (ev.type==ConfigureNotify) {
      /* Draw the window */
      draw_window(disp, win, id, im);
    }
  }
}
