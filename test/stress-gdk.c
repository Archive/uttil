/*
** UTTIL Gdk: Uniform Transparent/Translucent Imaging Library (Gdk)
** Copyright (c) 1999, Dylan R. Evans/t'Sade
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

/***** Includes *****/
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/shape.h>
#include <gdk_imlib.h>
#include "../uttil_imlib/uttil_imlib.h"
#include "util.h"

/***** Globals *****/
int shade = 0;
unsigned long mask = 0xFFFFFF;

/********** Main **********/
void draw_window(GdkWindow *win)
{
  /* Variables */
  GdkPixmap *u_pix, *im_pix, *im_msk;
  int dummy, w, h, d;
  Window dw;
  GdkImlibImage *im;
  struct timeval start_time;

  /* Get the time */
  gettimeofday(&start_time, NULL);

  /* Set the background */
  uttil_gdk_set_window_desktop_pixmap(win);

  /* Flush */
  gdk_window_clear(win);
  gdk_flush();

  /* All done */
  report_time(&start_time);
}

int main(int argc, char **argv)
{
  /* Variables */
  GdkWindowAttr         attr;
  GdkWindow             *win;
  int w = 200, h = 200;

  /* Print out a usage */
  printf("USAGE: %s [shade:integer] [mask:hex 'ffffff']\n",
	 argv[0]);
     
  /* Inititalise GDK */
  gdk_init(&argc,&argv);
  gdk_imlib_init();

  /* Get gdk to use imlib's visual and colormap */
  gtk_widget_push_visual(gdk_imlib_get_visual());
  gtk_widget_push_colormap(gdk_imlib_get_colormap());

  if(argc >= 2) {
    /* Shade */
    shade = atoi(argv[1]);
  }
  if(argc >= 3) {
    /* Mask */
    mask = strtol(argv[2], NULL, 16);
  }

  /* Set up attributes for GDK to create a Window */
  attr.window_type=GDK_WINDOW_TOPLEVEL;
  attr.wclass=GDK_INPUT_OUTPUT;
  attr.event_mask=GDK_STRUCTURE_MASK;
  attr.width=w;
  attr.height=h;

  /* Create a Window to display in */
  win = gdk_window_new(NULL,&attr,0);

  /* Display the window */
  draw_window(win);

  /* Actually display the window */
  gdk_window_show(win);

  /* Flush the GDK buffer */
  gdk_flush();

  /* Event loop to handle resizes */   
  for(;;) {
    /* Variables */
    GdkEvent *ev;
    
    /* Drag the event out of the Event queue */
    ev = gdk_event_get();

    /* Draw the window */
    draw_window(win);
  }
}
