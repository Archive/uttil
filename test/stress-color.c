/*
** UTTIL: Uniform Transparent/Translucent Imaging Library
** Copyright (c) 1999, Dylan R. Evans/t'Sade
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

/***** Includes *****/
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/shape.h>
#include "../uttil/uttil.h"
#include "util.h"

/***** Defines *****/
#define INIT_W 200
#define INIT_H 200

/***** Globals *****/
int shade = 0;
unsigned long mask = 0xFFFFFF;

/********** Main **********/
void draw_window(Display *disp, Window win)
{
  /* Variables */
  Pixmap p;
  struct timeval start_time;

  /* Get the time */
  gettimeofday(&start_time, NULL);

  /* Get the pixmap */
  p = uttil_get_window_desktop_pixmap(disp, win);
  if(p == None)
    return;

  /* Colormod it */
  uttil_colormod(disp, win, p, shade, mask);

  /* Set the background */
  XSetWindowBackgroundPixmap(disp, win, p);

  /* Free it */
  XFreePixmap(disp, p);

  /* Report it */
  report_time(&start_time);
}

int main(int argc, char **argv)
{
  Display *disp;
  XSetWindowAttributes attr;
  Window win, dw;
  Pixmap p,m, dp;
  int w,h;

  /* Make noise */
  printf("USAGE: %s <shade:integer> <mas:hex>\n", *argv);

  /* Do some command line options */
  if(argc != 3) {
    exit(1);
  }

  /* Convert it */
  printf("Attempting to get shade, red, green, and blue.\n");
  shade = atoi(argv[1]);
  mask = strtol(argv[2], NULL, 16);
  printf("Done. shade=%d, mask=0x%08x\n", shade, mask);

  /* Connect to the default Xserver */
  disp=XOpenDisplay(NULL);

  /* Just create the windows */
  win = XCreateWindow(disp, DefaultRootWindow(disp), 0, 0,
		      INIT_W, INIT_H, 0,
		      DefaultDepth(disp, DefaultScreen(disp)),
		      InputOutput,
		      DefaultVisual(disp, DefaultScreen(disp)),
		      0, &attr);
  XStoreName(disp, win, "Translucent Colormod Demo");

  /* Tell what we want to see */
  XSelectInput(disp, win,
	       StructureNotifyMask | SubstructureNotifyMask | KeyPressMask);

  /* Put the background in */
  draw_window(disp, win);

  /* Actually display the window */
  XMapWindow(disp,win);

  /* Synchronise with the Xserver */
  XSync(disp,False);

  /* Event loop to handle resizes */   
  for(;;) {
    /* Variables */
    XEvent ev;
    
    /* Sit and wait for an event to happen */ 
    XCheckMaskEvent(disp, KeyPressMask, &ev);

    if(ev.type == KeyPress)
      break;

    /* Put the background in */
    draw_window(disp, win);
      
    /* Clear the window to update the background change */
    XClearWindow(disp,win);
    
    /* Synchronise with the Xserver */
    XSync(disp,False);
  }
}
