/*
** UTTIL: Uniform Transparent/Translucent Imaging Library
** Copyright (c) 1999, Dylan R. Evans/t'Sade
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

/***** Includes *****/
#include "util.h"

/*
** report_time
*/

void report_time(struct timeval *start_time)
{
  /* Variables */
  struct timeval end_time;
  double delta;

  /* Get the time */
  gettimeofday(&end_time, NULL);

  /* Calculate the time */
  delta = (end_time.tv_sec - start_time->tv_sec) * 1000 * 1000 +
    (end_time.tv_usec - start_time->tv_usec);
  delta /= 1000 * 1000;

  /* Print it */
  fprintf(stderr, "Redraw: %f seconds.\n", delta);
}
