/*
** UTTIL Imlib: Uniform Transparent/Translucent Imaging Library (Imlib)
** Copyright (c) 1999, Dylan R. Evans/t'Sade
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 2
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

/***** Includes *****/
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/extensions/shape.h>
#include <Imlib.h>
#include "../uttil_imlib/uttil_imlib.h"
#include "util.h"

/***** Globals *****/
int shade = 0;
unsigned long mask = 0xFFFFFF;

/********** Main **********/
void draw_window(Display *disp, Window win, ImlibData *id)
{
  /* Variables */
  Pixmap u_pix, im_pix, im_msk;
  int dummy, w, h, d;
  Window dw;
  ImlibImage *im;
  struct timeval start_time;

  /* Get the time */
  gettimeofday(&start_time, NULL);

  /* Get the geometry of the window */
  XGetGeometry(disp, win, &dw, &dummy, &dummy, &w, &h, &dummy, &d);

  /*
  ** Get the background
  */

  /* Get the background pixmap for this one */
  u_pix = uttil_get_window_desktop_pixmap(disp, win);

  /* Convert it to an image */
  im = Imlib_create_image_from_drawable(id, u_pix, 0, 0, 0, w, h);

  /* Do a color mod on the included picture only */
  uttil_imlib_colormod(id, im, shade, mask);

  /* Render the original 24-bit Image data into a pixmap of size w * h */
  Imlib_render(id, im, w, h);

  /* Extract the Image and mask pixmaps from the Image */
  im_pix = Imlib_move_image(id, im);

  /* The mask will be 0 if the image has no transparency */
  im_msk = Imlib_move_mask(id, im);

  /* Put the Image pixmap in the background of the window */
  XSetWindowBackgroundPixmap(disp, win, im_pix);

  /* If there was a mask to the image, set the Image's mask to it */
  if(im_msk)
    XShapeCombineMask(disp, win, ShapeBounding, 0, 0, im_msk, ShapeSet);

  /* Sync */
  XClearWindow(disp, win);
  XFreePixmap(disp, im_pix);
  XFreePixmap(disp, im_msk);
  XFreePixmap(disp, u_pix);
  Imlib_destroy_image(id, im);
  XSync(disp, False);

  /* Alld one */
  report_time(&start_time);
}

int main(int argc, char **argv)
{
  /* Variables */
  Display *disp;
  ImlibData *id;
  XSetWindowAttributes attr;
  Window win;
  int w = 200, h = 200;

  /* Print out a usage */
  printf("USAGE: %s [shade:integer] [mask:hex 'ffffff']\n",
	 argv[0]);
     
  /* Connect to the default Xserver */
  disp=XOpenDisplay(NULL);

  /* Immediately afterwards Intitialise Imlib */
  id=Imlib_init(disp);

  if(argc >= 2) {
    /* Shade */
    shade = atoi(argv[1]);
  }
  if(argc >= 3) {
    /* Mask */
    mask = strtol(argv[2], NULL, 16);
  }

  /* Create a Window to display in */
  win = XCreateWindow(disp, DefaultRootWindow(disp), 0, 0, w, h, 0,
		      id->x.depth,
		      InputOutput, id->x.visual, 0, &attr);
  XSelectInput(disp, win, StructureNotifyMask|KeyPressMask);
  XStoreName(disp, win, "Imlib Colormod Desktop Demo");

  /* Display the window */
  draw_window(disp, win, id);

  /* Actually display the window */
  XMapWindow(disp,win);

  /* Event loop to handle resizes */   
  for(;;) {
    /* Variables */
    XEvent ev;

    /* Check the mask */
    XCheckMaskEvent(disp, KeyPressMask, &ev);

    if(ev.type == KeyPress)
      break;

    /* Draw the window */
    draw_window(disp, win, id);
  }
}
