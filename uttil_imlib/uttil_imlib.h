/*
** UTTIL Imlib: Uniform Transparent/Translucent Imaging Library (Imlib)
** Copyright (c) 1999, Dylan R. Evans/t'Sade
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA  02111-1307, USA.
*/

/***** Sentry *****/
#ifndef UTTIL_IMLIB_H
#define UTTIL_IMLIB_H

/***** Defines *****/
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xfuncproto.h>
#include <X11/Xatom.h>
#include <Imlib.h>
#include <uttil/uttil.h>

/***** Color Prototypes *****/
void uttil_imlib_colormod(ImlibData *, ImlibImage *, int, unsigned long);
/*void uttil_imlib_custom_colormod();*/

/***** Sentry *****/
#endif
