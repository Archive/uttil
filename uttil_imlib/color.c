/*
** UTTIL Imlib: Uniform Transparent/Translucent Imaging Library (Imlib)
** Copyright (c) 1999, Dylan R. Evans/t'Sade
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA  02111-1307, USA.
*/

/***** Includes *****/
#include "uttil_imlib.h"

/*
** uttil_imlib_colormod
*/

void
uttil_imlib_colormod(ImlibData *id, ImlibImage *im,
		     int rs_shadePct,
		     unsigned long rs_tintMask)
{
  /* Variables */
  unsigned int bright;
  ImlibColorModifier xform = {0xff, 0xff, 0xff};
  ImlibColorModifier rx = {0xff, 0xff, 0xff};
  ImlibColorModifier gx = {0xff, 0xff, 0xff};
  ImlibColorModifier bx = {0xff, 0xff, 0xff};
				
  /* Ignore it if we aren't doing anything */
  if(rs_shadePct == 0 && rs_tintMask == 0xffffff) {
    /* Don't bother */
    return;
  }

  /* Do the shading */
  if(rs_shadePct != 0) {
    /* Calculate the brightness */
    bright = 0xff - ((rs_shadePct * 0xff) / 100);
    xform.brightness = bright;

    /* Modify it */
    Imlib_set_image_modifier(id, im, &xform);
  }

  /* Do the tinting */
  if (rs_tintMask != 0xffffff) {
    /* Calculate the masks */
    rx.brightness = (rs_tintMask & 0xff0000) >> 16;
    gx.brightness = (rs_tintMask & 0x00ff00) >> 8;
    bx.brightness = rs_tintMask & 0x0000ff;

    /* Modify the colors */
    Imlib_set_image_red_modifier(id, im, &rx);
    Imlib_set_image_green_modifier(id, im, &gx);
    Imlib_set_image_blue_modifier(id, im, &bx);
  }
}
