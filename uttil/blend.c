/*
** UTTIL: Uniform Transparent/Translucent Imaging Library
** Copyright (c) 1999, Dylan R. Evans/t'Sade
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA  02111-1307, USA.
*/

/***** Includes *****/
#include "uttil.h"

/*
** uttil_blend_p_p
*/

Pixmap
uttil_blend_p_p(Display *disp,
		Pixmap p1, Pixmap p2, Pixmap rp)
{
  /* Variables */
  XImage *xim1, *xim2;
  GC gc;
  XGCValues gcvalues;
  int w, h, dummy;
  Window dw;

  /* Trace */
  uttil_trace(UTTIL_TRACE_FUNC, "+uttil_blend_p_p: p1=%p, p2=%p, rp=%p\n",
	      p1, p2, rp);

  /* Get the geometry of the result pixmap */
  uttil_trace(UTTIL_TRACE_INFUNC, "  XGetGeometry: disp=%p, rp=%p\n",
	      disp, rp);
  XGetGeometry(disp, rp, &dw, &dummy, &dummy, &w, &h, &dummy, &dummy);

  /* Get the XImage *'s */
  uttil_trace(UTTIL_TRACE_INFUNC, "  XGetImage: p1\n");
  xim1 = XGetImage(disp, p1, 0, 0, w, h, -1, ZPixmap);
  if(xim1 == NULL) {
    uttil_trace(UTTIL_TRACE_NOTICE,
		"%s: Could not get XImage * from Pixmap 1 (%p).\n",
		"uttil_blend_p_p",
		p1);
    uttil_trace(UTTIL_TRACE_FUNC, "-uttil_blend_p_p: No xim1\n");
    return p2;
  }

  uttil_trace(UTTIL_TRACE_INFUNC, "  XGetImage: p2\n");
  xim2 = XGetImage(disp, p2, 0, 0, w, h, -1, ZPixmap);
  if(xim2 == NULL) {
    uttil_trace(UTTIL_TRACE_NOTICE,
		"%s: Could not get XImage * from Pixmap 2 (%p).\n",
		"uttil_blend_p_p",
		p2);
    uttil_trace(UTTIL_TRACE_FUNC, "-uttil_blend_p_p: No xim2\n");
    return p1;
  }

  /* Blend them */
  uttil_trace(UTTIL_TRACE_INFUNC, "  uttil_blend_x_x\n");
  xim1 = uttil_blend_x_x(disp, xim1, xim2, xim1);

  /* Create the graphics context we need */
  uttil_trace(UTTIL_TRACE_INFUNC, "  XCreateGC: disp=%p, rp=%p\n", disp, rp);
  gc = XCreateGC(disp, rp, 0, &gcvalues);

  /* Put it back */
  uttil_trace(UTTIL_TRACE_INFUNC, "  XPutImage: gc=%p, xim1=%p\n", gc, xim1);
  XPutImage(disp, rp, gc, xim1, 0, 0, 0, 0, w, h);

  /* Destroy things */
  uttil_trace(UTTIL_TRACE_INFUNC, "  Cleanup block\n");
  XFreeGC(disp, gc);
  XDestroyImage(xim1);
  XDestroyImage(xim2);

  /* Return it */
  uttil_trace(UTTIL_TRACE_FUNC, "-uttil_blend_p_p: success\n");
  return rp;
}

/*
** uttil_blend_x_x
*/

XImage *
uttil_blend_x_x(Display *disp, XImage *xim1, XImage *xim2, XImage *xim3)
{
  /* Variables */
  int w, h, bd, j, i, depth;
  int x, y, ox, oy;

  /* Set defaults */
  x = y = ox = oy = 0;

  /* Get the X values */
  depth = DefaultDepth(disp, DefaultScreen(disp));

  /* Check to see if we got invalid images */
  if(!xim1) return xim2;
  if(!xim2) return xim1;

  /* Get the smallest size of the two images */
  w = xim3->width;
  h = xim3->height;

  if(xim1->width < w) w = xim1->width;
  if(xim2->width < w) w = xim2->width;

  if(xim1->height < h) h = xim1->height;
  if(xim2->height < h) h = xim2->height;

  /* Figure out the bit depth */
  bd = xim3->bits_per_pixel;

  /* Switch based on how big the pixel is */
  switch(bd) {
    /*
    ** 32-bit images
    */
  case 32:
    for(j = 0; j < h; j++) {
      /* Pointers */
      unsigned int *ptr1, *ptr2, *ptr3;

      /* Do some shifting */
      ptr1 = (unsigned int *)
	(xim1->data +
	 ((x) * ((xim1->bits_per_pixel) >> 3)) +
	 ((j + y) * xim1->bytes_per_line));
      ptr2 = (unsigned int *)
	(xim2->data +
	 ((ox) * ((xim2->bits_per_pixel) >> 3)) +
	 ((j + oy) * xim2->bytes_per_line));
      ptr3 = (unsigned int *)
	(xim3->data +
	 ((ox) * ((xim3->bits_per_pixel) >> 3)) +
	 ((j + oy) * xim3->bytes_per_line));

      /* Go through the row */
      for (i = 0; i < w; i++) {
	/* Variables */
	unsigned int p1, p2;

	/* Get the values */
	p1 = *ptr1++;
	p2 = *ptr2++;

	/* Figure out the blended value */
	*ptr3++ = ((p1 >> 1) & 0x7f7f7f7f) + ((p2 >> 1) & 0x7f7f7f7f) +
	  (p1 & p2 & 0x01010101);
      }
    }
    break;

    /*
    ** 16- and 15- bit images
    */
  case 16:
    /* Check the depth */
    if(depth != 15) { /* 16-bit */
      /* Go through the columns */
      for(j = 0; j < h; j++) {
	/* Variables */
	unsigned int *ptr1, *ptr2, *ptr3;

	/* Assign the values */
	ptr1 = (unsigned int *)
	  (xim1->data +
	   ((x) * ((xim1->bits_per_pixel) >> 3)) +
	   ((j + y) * xim1->bytes_per_line));
	ptr2 = (unsigned int *)
	  (xim2->data +
	   ((ox) * ((xim2->bits_per_pixel) >> 3)) +
	   ((j + oy) * xim2->bytes_per_line));
	ptr3 = (unsigned int *)
	  (xim3->data +
	   ((ox) * ((xim3->bits_per_pixel) >> 3)) +
	   ((j + oy) * xim3->bytes_per_line));

	/* Go through the width */
	if(!(w & 0x1)) {
	  /* Go through the rows */
	  for (i = 0; i < w; i += 2) {
	    /* Variables */
	    unsigned int p1, p2;

	    /* Assign the values */
	    p1 = *ptr1++;
	    p2 = *ptr2++;

	    /* Figure out the alpha */
	    *ptr3++ =
	      ((p1 >> 1) & ((0x78 << 8) | (0x7c << 3) | (0x78 >> 3) |
			    (0x78 << 24) | (0x7c << 19) | (0x78 << 13))) +
	      ((p2 >> 1) & ((0x78 << 8) | (0x7c << 3) | (0x78 >> 3) |
			    (0x78 << 24) | (0x7c << 19) | (0x78 << 13))) +
	      (p1 & p2 & ((0x1 << 11) | (0x1 << 5) | (0x1) |
			  (0x1 << 27) | (0x1 << 21) | (0x1 << 16)));
	  }
	}
	else {
	  /* Go through the rows */
	  for(i = 0; i < (w - 1); i += 2) {
	    /* Variables */
	    unsigned int p1, p2;

	    /* Get the values */
	    p1 = *ptr1++;
	    p2 = *ptr2++;

	    /* Figure out the alpha */
	    *ptr3++ =
	      ((p1 >> 1) & ((0x78 << 8) | (0x7c << 3) | (0x78 >> 3) |
			    (0x78 << 24) | (0x7c << 19) | (0x78 << 13))) +
	      ((p2 >> 1) & ((0x78 << 8) | (0x7c << 3) | (0x78 >> 3) |
			    (0x78 << 24) | (0x7c << 19) | (0x78 << 13))) +
	      (p1 & p2 & ((0x1 << 11) | (0x1 << 5) | (0x1) |
			  (0x1 << 27) | (0x1 << 21) | (0x1 << 16)));
	  }

	  {
	    /* Variables */
	    unsigned short *pptr1, *pptr2, *pptr3;
	    unsigned short pp1, pp2;
	    
	    /* Figure out the pointers */
	    pptr1 = (unsigned short *)ptr1;
	    pptr2 = (unsigned short *)ptr2;
	    pptr3 = (unsigned short *)ptr3;

	    /* Get the data */
	    pp1 = *pptr1;
	    pp2 = *pptr2;

	    /* Figure out the alpha */
	    *pptr3 = ((pp1 >> 1) & ((0x78 << 8) | (0x7c << 3) | (0x78 >> 3))) +
	      ((pp2 >> 1) & ((0x78 << 8) | (0x7c << 3) | (0x78 >> 3))) +
	      (pp1 & pp2 & ((0x1 << 11) | (0x1 << 5) | (0x1)));
	  }
	}
      }
    }
    else {/* 15-bit images */
      /* Go through the columns */
      for (j = 0; j < h; j++) {
	/* Variables */
	unsigned int *ptr1, *ptr2, *ptr3;
	    
	/* Assign the values */
	ptr1 = (unsigned int *)
	  (xim1->data +
	   ((x) * ((xim1->bits_per_pixel) >> 3)) +
	   ((j + y) * xim1->bytes_per_line));
	ptr2 = (unsigned int *)
	  (xim2->data +
	   ((ox) * ((xim2->bits_per_pixel) >> 3)) +
	   ((j + oy) * xim2->bytes_per_line));
	ptr3 = (unsigned int *)
	  (xim3->data +
	   ((ox) * ((xim3->bits_per_pixel) >> 3)) +
	   ((j + oy) * xim3->bytes_per_line));

	/* Check for even number of columns */
	if(!(w & 0x1)) {
	  /* Go through the rows */
	  for (i = 0; i < w; i += 2) {
	    /* Variables */
	    unsigned int p1, p2;
		    
	    /* Get the values */
	    p1 = *ptr1++;
	    p2 = *ptr2++;

	    /* Figure out the alpha */
	    *ptr3++ =
	      ((p1 >> 1) & ((0x78 << 7) | (0x78 << 2) | (0x78 >> 3) |
			    (0x78 << 23) | (0x78 << 18) | (0x78 << 13))) +
	      ((p2 >> 1) & ((0x78 << 7) | (0x78 << 2) | (0x78 >> 3) |
			    (0x78 << 23) | (0x78 << 18) | (0x78 << 13))) +
	      (p1 & p2 & ((0x1 << 10) | (0x1 << 5) | (0x1) |
			  (0x1 << 26) | (0x1 << 20) | (0x1 << 16)));
	  }
	}
	else { /* Odd number of columns */
	  for(i = 0; i < (w - 1); i += 2) {
	    /* Variables */
	    unsigned int p1, p2;
		    
	    /* Get the values */
	    p1 = *ptr1++;
	    p2 = *ptr2++;

	    /* Figure out the alpha */
	    *ptr3++ =
	      ((p1 >> 1) & ((0x78 << 7) | (0x78 << 2) | (0x78 >> 3) |
			    (0x78 << 23) | (0x78 << 18) | (0x78 << 13))) +
	      ((p2 >> 1) & ((0x78 << 7) | (0x78 << 2) | (0x78 >> 3) |
			    (0x78 << 23) | (0x78 << 18) | (0x78 << 13))) +
	      (p1 & p2 & ((0x1 << 10) | (0x1 << 5) | (0x1) |
			  (0x1 << 26) | (0x1 << 20) | (0x1 << 16)));
	  }

	  {
	    /* More variables */
	    unsigned short *pptr1, *pptr2, *pptr3;
	    unsigned short  pp1, pp2;
	    
	    /* Figure out where the pointers are */
	    pptr1 = (unsigned short *)ptr1;
	    pptr2 = (unsigned short *)ptr2;
	    pptr3 = (unsigned short *)ptr3;

	    /* Get the values */
	    pp1 = *pptr1;
	    pp2 = *pptr2;

	    /* Figure out the alpha */
	    *pptr3++ = ((pp1 >> 1) & ((0x78 << 7) | (0x78 << 2) | (0x78 >> 3))) +
	      ((pp2 >> 1) & ((0x78 << 7) | (0x78 << 2) | (0x78 >> 3))) +
	      (pp1 & pp2 & ((0x1 << 10) | (0x1 << 5) | (0x1)));
	  }
	}
      }
    }
    break;

    /*
    ** Everything Else
    */
  default:
    /* Go through the rows */
    for(j = 0; j < h; j++) {
      /* Variables */
      unsigned char *ptr1, *ptr2, *ptr3;

      /* Get the values */
      ptr1 = (unsigned char *)
	(xim1->data +
	 ((x) * ((xim1->bits_per_pixel) >> 3)) +
	 ((j + y) * xim1->bytes_per_line));
      ptr2 = (unsigned char *)
	(xim2->data +
	 ((ox) * ((xim2->bits_per_pixel) >> 3)) +
	 ((j + oy) * xim2->bytes_per_line));
      ptr3 = (unsigned char *)
	(xim3->data +
	 ((ox) * ((xim3->bits_per_pixel) >> 3)) +
	 ((j + oy) * xim3->bytes_per_line));

      /* Check for odd or even number of columns */
      if(!(w & 0x1)) { /* Even */
	/* More odd/even stuff */
	if(j & 0x1) { /* Even */
	  /* Increment */
	  ptr2++;

	  /* Go through the columns */
	  for(i = 0; i < w; i += 2) {
	    /* Variables */
	    unsigned char p1;

	    /* Build the alpha */
	    p1 = *ptr1;
	    ptr1 += 2;
	    *ptr3++ = p1;
	    p1 = *ptr2;
	    ptr2 += 2;
	    *ptr3++ = p1;
	  }
	}
	else {
	  /* Increment */
	  ptr1++;

	  /* Go through the columns */
	  for(i = 0; i < w; i += 2) {
	    /* Variables */
	    unsigned char p1;

	    /* Build the alpha */
	    p1 = *ptr2;
	    ptr2 += 2;
	    *ptr3++ = p1;
	    p1 = *ptr1;
	    ptr1 += 2;
	    *ptr3++ = p1;
	  }
	}
      }
      else { /* Odd */
	/* More odd-even stuff */
	if (j & 0x1) { /* Even */
	  /* Increment the pointer */
	  ptr2++;

	  /* Go through the columns */
	  for(i = 0; i < (w - 1); i += 2) {
	    /* Variables */
	    unsigned char p1;

	    /* Build the alpha */
	    p1 = *ptr1;
	    ptr1 += 2;
	    *ptr3++ = p1;
	    p1 = *ptr2;
	    ptr2 += 2;
	    *ptr3++ = p1;
	  }

	  /* Set the data */
	  *ptr3 = *ptr1;
	}
	else { /* Odd */
	  /* Increment */
	  ptr1++;

	  /* Go through the columns */
	  for(i = 0; i < (w - 1); i += 2) {
	    /* Variables */
	    unsigned char p1;

	    /* Build the alpha */
	    p1 = *ptr2;
	    ptr2 += 2;
	    *ptr3++ = p1;
	    p1 = *ptr1;
	    ptr1 += 2;
	    *ptr3++ = p1;
	  }

	  /* Set the data */
	  *ptr3 = *ptr2;
	}
      }
    }
    break;
  }

  /* Return the one we have been changing */
  return xim3;
}

#if 0
EBlendPixImg(EWin * ewin, PixImg * s1, PixImg * s2, PixImg * dst, int x, int y, int w, int h)
{
   int                 ox, oy;
   int                 i, j;
   XGCValues           gcv;
   static int          rn, ord;
   static XRectangle  *rl = NULL;
   static GC           gc = 0;

   if (dst)
     {
       switch (xim3->bits_per_pixel)
	 {
	 case 16:
	 default:
}

#endif

#if 0
  /* Variables */
  int w, h, bd, j, i;

  /* Check to see if we got invalid images */
  if(!xim1) return xim2;
  if(!xim2) return xim1;

  /* Get the smallest size of the two images */
  w = (xim1->width < xim2->width) ? xim1->width : xim2->width;
  h = (xim1->height < xim2->height) ? xim1->height : xim2->height;

  /* Figure out the bit depth */
  bd = xim1->bits_per_pixel;

  /* Switch based on how big the pixel is */
  switch(bd) {
  case 32: /* 32 bits / pixel */
    for(j = 0; j < h; j++) {
      /* Pointers */
      unsigned int *ptr1, *ptr2;

      /* Get the row of pixels */
      ptr1 = (unsigned int *)(xim1->data + (j * xim1->bytes_per_line));
      ptr2 = (unsigned int *)(xim2->data + (j * xim2->bytes_per_line));

      for(i = 0; i < w; i++) {
	/* Data */
	unsigned int p1, p2;

	/* Get the pixels */
	p1 = *ptr1++;
	p2 = *ptr2;

	/* Move the second pointer */
	*ptr2++ = ((p1 >> 1) & 0x7f7f7f7f) + ((p2 >> 1) & 0x7f7f7f7f) +
	  (p1 & p2 & 0x01010101);
      }
    }
    break;
  case 16: /* 16 bit pixels */
    /* Go through the rows */
    for (j = 0; j < h; j++) {
      /* Pointers to the row of pixels */
      unsigned short *ptr1, *ptr2;

      /* Get the row of pixels */
      ptr1 = (unsigned short *)(xim1->data + (j * xim1->bytes_per_line));
      ptr2 = (unsigned short *)(xim2->data + (j * xim2->bytes_per_line));

      /* Go through the row */
      for (i = 0; i < w; i++) {
	/* Actual pixels */
	unsigned short p1, p2;

	/* Get the pixels */
	p1 = *ptr1++;
	p2 = *ptr2;

	/* Move down the row */
	*ptr2++ = ((p1 >> 1) & ((0x78 << 8) |
				(0x7c << 3) |
				(0x78 >> 3))) +
	  ((p2 >> 1) & ((0x78 << 8) | (0x7c << 3) | (0x78 >> 3))) +
	  (p1 & p2 & ((0x1 << 11) | (0x1 << 5) | (0x1)));
      }
    }
    break;
  default:
    /* Go through the row */
    for(j = 0; j < h; j++) {
      /* Row Data */
      unsigned char *ptr1, *ptr2;

      /* Get the row of pixels */
      ptr1 = (unsigned char *)(xim1->data + (j * xim1->bytes_per_line));
      ptr2 = (unsigned char *)(xim2->data + (j * xim2->bytes_per_line));

      /* Move things */
      if(j & 0x1) {
	/* Move it forward */
	ptr1++;
	ptr2++;
      }

      /* Go through this row */
      for(i = 0; i < w; i += 2) {
	/* Data */
	unsigned char p1;
	
	/* Do it */
	p1 = *ptr1;
	ptr1 += 2;
	*ptr2 = p1;
	ptr2 += 2;
      }
    }
    break;
  }

  /* Return the one we have been changing */
  return xim3;
#endif
