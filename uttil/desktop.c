/*
** UTTIL: Uniform Transparent/Translucent Imaging Library
** Copyright (c) 1999, Dylan R. Evans/t'Sade
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA  02111-1307, USA.
*/

/***** Includes *****/
#include "uttil.h"

/***** Globals *****/
#ifdef USE_STATIC_CACHE
Pixmap desktop_pixmap = None;
#endif

/*
** uttil_get_win_desktop
*/

Window
uttil_get_window_desktop(Display *disp, Window win)
{
  /* Variables */
  Atom prop1, type, prop2;
  int format;
  unsigned long length, after;
  unsigned char *data;
  unsigned int nchildren;
  Window w, root, *children, parent;

  /* Get the properties we care about */
  prop1 = XInternAtom(disp, "_XROOTPMAP_ID", True);
  prop2 = XInternAtom(disp, "_XROOTCOLOR_PIXEL", True);

  /* Check for nothing found */
  if(prop1 == None && prop2 == None) {
    /* We couldn't identify the atoms */
    return None;
  }
     
  /* Go through the items, looking for the desktop win. Start with
  ** this one in the odd chance its actually the desktop win. */

  /* Start with this one */
  w = win;

  /* Go through the wins, looking for parents */
  for(w = win; w; w = parent) {
    /* Query the X tree */
    if((XQueryTree(disp, w,
		   &root, &parent, &children, &nchildren)) == False) {
      /* Couldn't query the tree */
      return None;
    }

    /* If there are any children, get rid of them since we
    ** don't need to know about them. */
    if(nchildren) {
      XFree(children);
    }

    /* Check for the pixmap or pixels */
    if(prop1 != None) {
      XGetWindowProperty(disp, w, prop1, 0L, 1L,
			 False, AnyPropertyType,
			 &type, &format, &length, &after, &data);
    }
    else if(prop2 != None) {
      XGetWindowProperty(disp, w, prop2, 0L, 1L,
			 False, AnyPropertyType,
			 &type, &format, &length, &after, &data);
    }
    else {
      /* Nothing of note, so keep going */
      continue;
    }

    /* Check to see if found anything */
    if(type != None) {
      /* This win appears to be actually it */
      return w;
    }
  }

  /* Nothing found */
  return None;
}

/*
** uttil_get_window_pixmap
**  Need to free the Pixmap.
*/

Pixmap
uttil_get_window_pixmap(Display *disp, Window win)
{
  /* Variables */
  Pixmap p = None, pix;
  GC gc;
  XGCValues gcvalues;
  Atom prop1, prop2, type;
  int format;
  unsigned long length, after;
  unsigned char *data;
  int pw, ph, ww, wh, wd, dummy;
  Window rw;

#ifdef USE_STATIC_CACHE
  /* Check the cache */
  if(desktop_pixmap == None) {
#endif

    /* Noise */
    UTTIL_TRACE((UTTIL_TRACE_FUNC,
		"+uttil_get_window_pixmap: disp=%p, win=%p\n",
		disp, win));
    
    /* Check for a blank win */
    if(win == None) {
      /* Nothing to do */
      UTTIL_TRACE((UTTIL_TRACE_FUNC,
		   "-uttil_get_window_pixmap: no window\n"));
      return None;
    }
    
    /* Get the atoms */
    prop1 = XInternAtom(disp, "_XROOTPMAP_ID", True);
    prop2 = XInternAtom(disp, "_XROOTCOLOR_PIXEL", True);
    
    if(prop1 == None && prop2 == None) {
      /* Can't do anything */
      UTTIL_TRACE((UTTIL_TRACE_FUNC,
		   "-uttil_get_window_pixmap: no atoms\n"));
      return None;
    }
    
    /* Pixmaps */
    if(prop1 != None) {
      /* Get information */
      XGetWindowProperty(disp, win, prop1, 0L, 1L,
			 False, AnyPropertyType,
			 &type, &format, &length, &after, &data);
      
      /* Check for a pixmap */
      if(type == XA_PIXMAP) {
	/* Get it */
	p = *((Pixmap *) data);
	UTTIL_TRACE((UTTIL_TRACE_INFUNC,
		     "  Found a desktop pixmap (%p)\n", p))
#ifdef USE_STATIC_CACHE
	desktop_pixmap = p;
#endif
      }
    }
    
    /* Solid Colors */
    else if(prop2 != None) {
      /* Get information */
      XGetWindowProperty(disp, win, prop2, 0L, 1L,
			 False, AnyPropertyType,
			 &type, &format, &length, &after, &data);
      
      /* Check for solid color */
      if(type == XA_CARDINAL) {
	/* Ignore it */
	UTTIL_TRACE((UTTIL_TRACE_FUNC,
		    "-uttil_get_window_pixmap: cardinal\n"))
	return None;
      }
    }

    else {
      /* Nothing found */
      UTTIL_TRACE((UTTIL_TRACE_FUNC,
		   "-uttil_get_window_pixmap: nothing\n"));
      return None;
    }
#ifdef USE_STATIC_CACHE
  }
  else {
    /* We have the desktop pixmap */
    p = desktop_pixmap;
  }
#endif
	
  /* Get sizes */
  XGetGeometry(disp, win, &rw, &dummy, &dummy, &ww, &wh, &dummy, &wd);
  XGetGeometry(disp, p, &rw, &dummy, &dummy, &pw, &ph, &dummy, &dummy);
  
  /* Create the outer pixmap */
  pix = XCreatePixmap(disp, win, ww, wh, wd);
  
  /* Create the gc */
  gc = XCreateGC(disp, win, 0, &gcvalues);
  
  /* Figure out if we need to make a full version or tile it */
#if 0
  if(pw < ww || ph < wh) {
#endif
    /* Set up the tile */
    XSetTile(disp, gc, p);
    XSetTSOrigin(disp, gc, 0, 0);
    XSetFillStyle(disp, gc, FillTiled);
    
    /* Put in the tiles */
    XFillRectangle(disp, pix, gc, 0, 0, ww, wh);
#if 0
  } else {
    /* Just copy it */
    XCopyArea(disp, p, pix, gc, 0, 0, ww, wh, 0, 0);
  }
#endif
  
  /* Free variables */
  XFreeGC(disp, gc);
  
  /* Return it */
  UTTIL_TRACE((UTTIL_TRACE_FUNC,
	      "-uttil_get_window_pixmap: sucess pix=%p\n", pix));
  return pix;
}

/*
** uttil_get_window_desktop_pixmap
*/

Pixmap uttil_get_window_desktop_pixmap(Display *disp, Window win)
{
  /* Variables */
  Window rw;
  int x, y, w, h, d, b;

  /* Get the geometry */
  XGetGeometry(disp, win, &rw, &x, &y, &w, &h, &b, &d);

  /* Call the larger version */
  return uttil_getv_window_desktop_pixmap(disp, win, x, y, w, h, d);
}

/*
** uttil_getv_window_desktop_pixmap
**  Need to free the Pixmap.
*/

Pixmap uttil_getv_window_desktop_pixmap(Display *disp, Window win,
				       int x, int y, int w, int h, int d)
{
  /* Variables */
  Pixmap dp, wp;
  Window dw, rw, cw;
  GC gc;
  XGCValues gcvalues;
  int px, py, pw, ph, pb, pd;
  int wx, wy, ww, wh, wb, wd;

  /* Noise */
  UTTIL_TRACE((UTTIL_TRACE_FUNC,
	      "+uttil_get_window_desktop_pixmap: disp=%p, win=%p\n",
	      disp, win));

  /* Get the desktop win */
  dw = uttil_get_window_desktop(disp, win);
  if(dw == None) {
    /* No desktop win */
    UTTIL_TRACE((UTTIL_TRACE_NOTICE,
		"Cannot get desktop win from win (%p).\n",
		win));
    UTTIL_TRACE((UTTIL_TRACE_FUNC,
		"-uttil_get_window_desktop_pixmap: no root window\n"));
    return None;
  }

  dp = uttil_get_window_pixmap(disp, dw);
  if(dp == None) {
    /* Get get pixmap from */
    UTTIL_TRACE((UTTIL_TRACE_NOTICE,
		"Cannot get pixmap from win (%p).\n",
		dw));
    UTTIL_TRACE((UTTIL_TRACE_FUNC,
		"-uttil_get_window_desktop_pixmap: no root pixmap\n"));
    return None;
  }

  /* Get the sizes of both the win we have and the
  ** pixmap we just got. */
  XGetGeometry(disp, win, &rw, &wx, &wy, &ww, &wh, &wb, &wd);
  XGetGeometry(disp, dp,  &rw, &px, &py, &pw, &ph, &pb, &pd);

  /* We need the win's coordinates based on the root win */
  XTranslateCoordinates(disp, win, rw, 0, 0, &wx, &wy, &cw);

  /* Make noise */
  UTTIL_TRACE((UTTIL_TRACE_INFO, "Win: win=%p, x=%d, y=%d, w=%d, h=%d\n",
	      win, wx, wy, ww, wh));
  UTTIL_TRACE((UTTIL_TRACE_INFO, "Pixmap: win=%p, x=%d, y=%d, w=%d, h=%d\n",
	      dp, px, py, pw, ph));

  /* Create a new pixmap of the proper size */
  wp = XCreatePixmap(disp, win, ww, wh, wd);

  /* Create the graphics context we need for the copy */
  gc = XCreateGC(disp, win, 0, &gcvalues);

  /* Pull out the area from under the win and put it in another
  ** pixmap which we'll use as the background. */
  XCopyArea(disp, dp, wp, gc, wx, wy, ww, wh, 0, 0);

  /* Free up some variables */
  XFreeGC(disp, gc);
  XFreePixmap(disp, dp);

  /* Return the pixmap */
  UTTIL_TRACE((UTTIL_TRACE_FUNC,
	      "-uttil_get_window_desktop_pixmap: success\n"));
  return wp;
}

/*
** uttil_set_window_desktop_pixmap
*/

int uttil_set_window_desktop_pixmap(Display *disp, Window win)
{
  /* Variables */
  int x, y, h, w, d, dummy;
  Window rw;

  /* Get the size of the window to be grabbed */
  XGetGeometry(disp, win, &rw, &x, &y, &w, &h, &dummy, &d);

  /* Return it */
  return uttil_setv_window_desktop_pixmap(disp, win, x, y, w, h, d);
}

/*
** uttil_setv_window_desktop_pixmap
*/

int uttil_setv_window_desktop_pixmap(Display *disp, Window win,
				     int x, int y,
				     int w, int h, int d)
{
  /* Variables */
  Pixmap wp;

  /* Get the win pixmap */
  wp = uttil_getv_window_desktop_pixmap(disp, win, x, y, w, h, d);
  if(wp == None) {
    /* Can't get it */
    UTTIL_TRACE((UTTIL_TRACE_NOTICE,
		"Cannot get pixmap of the desktop of the win (%p).\n",
		win));
    return 1;
  }

  /* Put the pixmap in the background */
  XSetWindowBackgroundPixmap(disp, win, wp);

  /* Free up some variables */
  XFreePixmap(disp, wp);

  /* Return no problems */
  return 0;
}
