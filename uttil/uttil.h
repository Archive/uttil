/*
** UTTIL: Uniform Transparent/Translucent Imaging Library
** Copyright (c) 1999, Dylan R. Evans/t'Sade
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA  02111-1307, USA.
*/

/***** Sentry *****/
#ifndef UTTIL_H
#define UTTIL_H

/***** Defines *****/
#include <stdio.h>
#include <X11/X.h>
#include <X11/Xlib.h>
#include <X11/Xfuncproto.h>
#include <X11/Xatom.h>

/***** Structures *****/
struct _Colortable {
  int r, g, b;
  int pixel;
};
typedef struct _Colortable Colortable;

/***** Blend Prototypes *****/
Pixmap uttil_blend_p_p(Display *, Pixmap, Pixmap, Pixmap);
XImage *uttil_blend_x_x(Display *, XImage *, XImage *, XImage *);

/***** Color Prototypes *****/
void uttil_colormod(Display *, Window, Pixmap, int, unsigned long);
int uttil_best_color_match(Display *, Colortable *, int, int, int, int);

/***** Desktop Prototypes *****/
Window uttil_get_window_desktop(Display *, Window);
Pixmap uttil_get_window_pixmap(Display *, Window);

Pixmap uttil_get_window_desktop_pixmap(Display *, Window);
Pixmap uttil_getv_window_desktop_pixmap(Display *, Window,
					int, int, int, int, int);

int uttil_set_window_desktop_pixmap(Display *, Window);
int uttil_setv_window_desktop_pixmap(Display *, Window,
				     int, int, int, int, int);

/***** Utility Prototypes *****/
#ifdef DEBUG
#define UTTIL_TRACE(x) uttil_trace(x)
void uttil_trace(long, char *, ...);
#else
#define UTTIL_TRACE(x) 
#endif


/***** Trace Defines *****/
#define UTTIL_TRACE_FATAL    (1     )
#define UTTIL_TRACE_WARNING  (1 >> 1)
#define UTTIL_TRACE_NOTICE   (1 >> 2)
#define UTTIL_TRACE_INFO     (1 >> 3)
#define UTTIL_TRACE_FUNC     (1 >> 4)
#define UTTIL_TRACE_INFUNC   (1 >> 5)
#define UTTIL_TRACE_INFUNC1  (1 >> 6)

/***** Sentry *****/
#endif
