/*
** UTTIL: Uniform Transparent/Translucent Imaging Library
** Copyright (c) 1999, Dylan R. Evans/t'Sade
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA  02111-1307, USA.
*/

/***** Includes *****/
#include "uttil.h"

/*
** uttil_colormod
*/

void
uttil_colormod(Display *disp, Window win, Pixmap pix,
	       int rs_shadePct,
	       unsigned long rs_tintMask)
{
  /* Variables */
  XImage *xim;
  Colormap cmap;
  Window dw;
  GC gc;
  XGCValues gcvalues;

  register unsigned long v, i;
  unsigned long x, y;
  unsigned int r, g, b;
  int w, h, dummy;
  float rm, gm, bm, shade;
  Colortable ctab[256];
  int real_depth = 0;
  register int br, bg, bb;
  register unsigned int mr, mg, mb;
  int depth;

  /* Get the depths */
  depth = DefaultDepth(disp, DefaultScreen(disp));

  /* Ignore if its transparent, as opposed to translucent */
  if(rs_shadePct == 0 && rs_tintMask == 0xffffff) {
    return;
  }

  /* Get the geometry of the window */
  XGetGeometry(disp, win, &dw, &dummy, &dummy, &w, &h, &dummy, &depth);

  /*
  **
  ** Now we do the tinting based on depth of the screen (bits per pixel)
  **
  */

  /*
  ** 8-bit colors
  */

  if(depth <= 8) {
    /* Variable */
    XColor cols[256];

    for(i = 0; i < (1 << depth); i++) {
      cols[i].pixel = i;
      cols[i].flags = DoRed | DoGreen | DoBlue;
    }

    cmap = DefaultColormap(disp, DefaultScreen(disp));
    XQueryColors(disp, cmap, cols, 1 << depth);

    for(i = 0; i < (1 << depth); i++) {
      ctab[i].r = cols[i].red >> 8;
      ctab[i].g = cols[i].green >> 8;
      ctab[i].b = cols[i].blue >> 8;
      ctab[i].pixel = cols[i].pixel;
    }
  }


  /*
  ** 16-bit colors
  */
  else if (depth == 16) {
    /* Variables */
    XWindowAttributes xattr;

    XGetWindowAttributes(disp, win, &xattr);
    if((xattr.visual->red_mask == 0x7c00) &&
       (xattr.visual->green_mask == 0x3e0) &&
       (xattr.visual->blue_mask == 0x1f)) {
      real_depth = 15;
    }
  }

  /*
  ** Do some work on the original image, etc.
  */

  /* Check to see if the depth is different */
  if (!real_depth) {
    real_depth = depth;
  }

  /* Figure out some values */
  shade = (float) (100 - rs_shadePct) / 100.0;
  rm = (float) ((rs_tintMask & 0xff0000) >> 16) / 255.0 * shade;
  gm = (float) ((rs_tintMask & 0x00ff00) >> 8) / 255.0 * shade;
  bm = (float) (rs_tintMask & 0x0000ff) / 255.0 * shade;
  
  /* Get the image */
  xim = XGetImage(disp, pix, 0, 0, w, h, -1, ZPixmap);
  if (xim == NULL) {
    uttil_trace(UTTIL_TRACE_WARNING,
		"%s 0x%08x, 0, 0, %d, %d, -1, ZPixmap) returned Null.",
		"uttil_colormod: XGetImage(disp,",
		pix, w, h);
    return;
  }

  /*
  ** Less than 8-bit colors
  */
  if (depth <= 8) {
    for (y = 0; y < h; y++) {
      for (x = 0; x < w; x++) {
	v = XGetPixel(xim, x, y);
	r = (int) ctab[v & 0xff].r * rm;
	g = (int) ctab[v & 0xff].g * gm;
	b = (int) ctab[v & 0xff].b * bm;
	v = uttil_best_color_match(disp, ctab, depth, r, g, b);
	XPutPixel(xim, x, y, v);
      }
    }
  } else {
    /* Determine bitshift and bitmask values */
    switch (real_depth) {
    case 15:
      br = 7;
      bg = 2;
      bb = 3;
      mr = mg = mb = 0xf8;
      break;
    case 16:
      br = 8;
      bg = bb = 3;
      mr = mb = 0xf8;
      mg = 0xfc;
      break;
    case 24:
    case 32:
      br = 16;
      bg = 8;
      bb = 0;
      mr = mg = mb = 0xff;
      break;
    default:
      uttil_trace(UTTIL_TRACE_WARNING,
		  "%s %d is unsupported for tinting/shading.",
		  "uttil_colormod: Bit depth of",
		  real_depth);
      return;
    }
    
    for (y = 0; y < h; y++) {
      for (x = 0; x < w; x++) {
	v = XGetPixel(xim, x, y);
	r = (int) (((v >> br) & mr) * rm) & 0xff;
	g = (int) (((v >> bg) & mg) * gm) & 0xff;
	b = (int) (((v << bb) & mb) * bm) & 0xff;
	v = ((r & mr) << br) | ((g & mg) << bg) | ((b & mb) >> bb);
	XPutPixel(xim, x, y, v);
      }
    }
  }

  /* Create a graphics context */
  gc = XCreateGC(disp, win, 0, &gcvalues);

  /* Put it in the pixmap using the gc */
  XPutImage(disp, pix, gc, xim, 0, 0, 0, 0, w, h);

  /* Cleanup */
  XFreeGC(disp, gc);
  XDestroyImage(xim);
}

/*
** uttil_best_color_match
*/

int
uttil_best_color_match(Display *disp, Colortable *ctab,
		       int depth, int r, int g, int b)
{
  /* Variables */
  int i, dr, dg, db, dif, col;
  int mindif = 60000;

  /* Go through the color map */
  for(i = 0; i < (1 << depth); i++) {
    /* Get the delta for red */
    dr = r - ctab[i].r;
    if(dr < 0)
      dr = -dr;

    /* Get the delta for green */
    dg = g - ctab[i].g;
    if (dg < 0)
      dg = -dg;

    /* Get the delta for blue */
    db = b - ctab[i].b;
    if (db < 0)
      db = -db;

    /* Get the final delta */
    dif = dr + dg + db;
    if (dif < mindif) {
      mindif = dif;
      col = i;
    }
  }

  /* We have a final color */
  return ctab[col].pixel;
}
