/*
** UTTIL: Uniform Transparent/Translucent Imaging Library
** Copyright (c) 1999, Dylan R. Evans/t'Sade
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA  02111-1307, USA.
*/

/***** Includes *****/
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "uttil.h"

/*
** uttil_trace
*/

void uttil_trace(long lvl, char *fmt, ...)
{
  /* Variables */
  va_list args;
  long env_lvl = 0;
  char *env_str;

  /* Get the environment variable to see if we want logging. */
  env_str = getenv("UTTIL_LOG");
  if(env_str && *env_str) {
    /* Set it */
    env_lvl = atol(env_str);
  }

  /* Check for a level match (simple check to see if bits match) */
  if(env_lvl | lvl) {
    /* Get the variable arguments */
    va_start(args, fmt);
    
    /* Just print it */
    fprintf(stderr, "UTTIL: ");
    vfprintf(stderr, fmt, args);
    
    /* Finish it */
    va_end(args);
  }
}
