/*
** UTTIL Gdk: Uniform Transparent/Translucent Imaging Library (Gdk)
** Copyright (c) 1999, Dylan R. Evans/t'Sade
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA  02111-1307, USA.
*/

/***** Includes *****/
#include "uttil_gdk.h"

/*
** uttil_gdk_colormod_gp
*/

GdkPixmap *
uttil_gdk_colormod_gp(GdkPixmap *pix,
		     int rs_shadePct,
		     unsigned long rs_tintMask)
{
  /* Variables */
  GdkImlibImage *im;
  GdkPixmap *p;
  gint w, h;

  /* Trace */
  uttil_trace(UTTIL_TRACE_FUNC, "+uttil_gdk_colormod_gp: pix=%p\n",
	      pix);

  /* Ignore blanks */
  if(!pix) {
    uttil_trace(UTTIL_TRACE_FUNC, "-uttil_gdk_colormod_gp: blank pix\n");
    return 0;
  }

  /* Get the size of the window */
  uttil_trace(UTTIL_TRACE_INFUNC, "  gdk_window_get_geometry\n");
  gdk_window_get_geometry(pix, 0, 0, &w, &h, 0);

  /* Sanity check the sizes */
  if(w > 65000 || h > 65000) {
    /* Unlikely */
    uttil_trace(UTTIL_TRACE_FUNC, "-%s: too big: w=%d, h=%d\n",
		"uttil_gdk_colormod_gp", w, h);
    return 0;
  }

  /* Get the image from the pixmap */
  uttil_trace(UTTIL_TRACE_INFUNC, "  gdk_imlib_create_image_from_drawable\n");
  im = gdk_imlib_create_image_from_drawable(pix, 0, 0, 0, w, h);
  if(!im) {
    uttil_trace(UTTIL_TRACE_FUNC, "-uttil_gdk_colormod_gp: blank im\n");
    return 0;
  }

  /* Do the colormods */
  uttil_trace(UTTIL_TRACE_INFUNC, "  uttil_gdk_colormod_gim: im=%p\n", im);
  uttil_gdk_colormod_gim(im, rs_shadePct, rs_tintMask);

  /* Render it */
  uttil_trace(UTTIL_TRACE_INFUNC, "  gdk_imlib_render im=%p, w=%d, h=%d\n",
	      im, w, h);
  gdk_imlib_render(im, w, h);

  /* Pull it back out */
  uttil_trace(UTTIL_TRACE_INFUNC, "  gdk_imlib_move_image\n");
  p = gdk_imlib_move_image(im);

  /* Free things */
  uttil_trace(UTTIL_TRACE_INFUNC, "  gdk_imlib_destroy_image: p=%p\n", p);
  gdk_imlib_destroy_image(im);

  /* Return it */
  uttil_trace(UTTIL_TRACE_FUNC, "-uttil_gdk_colormod_gp: success\n");
  return p;
}

/*
** uttil_gdk_colormod_gim
*/

void
uttil_gdk_colormod_gim(GdkImlibImage *im,
		      int rs_shadePct,
		      unsigned long rs_tintMask)
{
  /* Variables */
  unsigned int bright;
  GdkImlibColorModifier xform = {0xff, 0xff, 0xff};
  GdkImlibColorModifier gx = {0xff, 0xff, 0xff};
  GdkImlibColorModifier bx = {0xff, 0xff, 0xff};
  GdkImlibColorModifier rx = {0xff, 0xff, 0xff};

  /* Ignore it if we aren't doing anything */
  if(rs_shadePct == 0 && rs_tintMask == 0xffffff) {
    /* Don't bother */
    return;
  }

  /* Do the shading */
  if(rs_shadePct != 0) {
    /* Calculate the brightness */
    bright = 0xff - ((rs_shadePct * 0xff) / 100);
    xform.brightness = bright;

    /* Modify it */
    gdk_imlib_set_image_modifier(im, &xform);
  }

  /* Do the tinting */
  if (rs_tintMask != 0xffffff) {
    /* Calculate the masks */
    rx.brightness = (rs_tintMask & 0xff0000) >> 16;
    gx.brightness = (rs_tintMask & 0x00ff00) >> 8;
    bx.brightness = rs_tintMask & 0x0000ff;

    /* Modify the colors */
    gdk_imlib_set_image_red_modifier(im, &rx);
    gdk_imlib_set_image_green_modifier(im, &gx);
    gdk_imlib_set_image_blue_modifier(im, &bx);
  }
}
