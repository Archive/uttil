/*
** UTTIL Gdk: Uniform Transparent/Translucent Imaging Library (Gdk)
** Copyright (c) 1999, Dylan R. Evans/t'Sade
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA  02111-1307, USA.
*/

/***** Includes *****/
#include "uttil_gdk.h"

/*
** uttil_gdk_blend_gp_gp
*/

GdkPixmap *
uttil_gdk_blend_gp_gp(GdkPixmap *gp1, GdkPixmap *gp2, GdkPixmap *gpr)
{
  /* Variables */
  GdkGC *ggc;
  GdkPixmap *np;
  gint w1, w2, wr;
  gint h1, h2, hr;
  gint d1, d2, dr;
  int dummy;
  GdkImage *gim1, *gim2, *gimr;
  XImage *xim1, *xim2, *ximr;

  /* Tracing */
  uttil_trace(UTTIL_TRACE_FUNC,
	      "+uttil_gdk_blend_gp_gp: gp1=%p, gp2=%p, gpr=%p\n",
	      gp1, gp2, gpr);

  /* Create a graphics context */
  ggc = gdk_gc_new(gpr);

  /* Get the sizes */
  gdk_window_get_geometry(gp1, 0, 0, &w1, &h1, &d1); 
  gdk_window_get_geometry(gp2, 0, 0, &w2, &h2, &d2);
  gdk_window_get_geometry(gpr, 0, 0, &wr, &hr, &dr);

  /* Get images from pixmaps */
  gim1 = gdk_image_get(gp1, 0, 0, w1, h1);
  gim2 = gdk_image_get(gp2, 0, 0, w2, h2);
  gimr = gdk_image_get(gpr, 0, 0, wr, hr);

  /* Get images from gdkimage */
  xim1 = GDK_IMAGE_XIMAGE(gim1);
  xim2 = GDK_IMAGE_XIMAGE(gim2);
  ximr = GDK_IMAGE_XIMAGE(gimr);

  /* Blend the images */
  ximr = uttil_blend_x_x(GDK_WINDOW_XDISPLAY(gpr), xim1, xim2, ximr);

  /* Put the resulting image back into gpr */
  gdk_draw_image(gpr, ggc, gimr, 0, 0, 0, 0, wr, hr);

  /* Free memory */
  gdk_gc_destroy(ggc);
  gdk_image_destroy(gim1);
  gdk_image_destroy(gim2);
  gdk_image_destroy(gimr);

  /* Return it */
  uttil_trace(UTTIL_TRACE_FUNC, "-uttil_gdk_blend_gp_gp: success\n");
  return gpr;
}
