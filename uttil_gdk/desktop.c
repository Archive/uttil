/*
** UTTIL Gdk: Uniform Transparent/Translucent Imaging Library (Gdk)
** Copyright (c) 1999, Dylan R. Evans/t'Sade
**
** This library is free software; you can redistribute it and/or
** modify it under the terms of the GNU Library General Public
** License as published by the Free Software Foundation; either
** version 2 of the License, or (at your option) any later version.
**
** This library is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
** Library General Public License for more details.
**
** You should have received a copy of the GNU Library General Public
** License along with this library; if not, write to the
** Free Software Foundation, Inc., 59 Temple Place - Suite 330,
** Boston, MA  02111-1307, USA.
*/

/***** Includes *****/
#include "uttil_gdk.h"

/*
** uttil_gdk_get_window_desktop
*/

GdkWindow *
uttil_gdk_get_window_desktop(GdkWindow *win)
{
  /* Return the handy function gdk has */
  return GDK_ROOT_PARENT();
}

/*
** uttil_gdk_get_window_pixmap
*/

GdkPixmap *
uttil_gdk_get_window_pixmap(GdkWindow *win)
{
  /* Variables */
  Pixmap p;
  GdkPixmap *gp;

  /* Make noise */
  uttil_trace(UTTIL_TRACE_FUNC, "+uttil_gdk_get_window_pixmap: win=%p\n",
	      win);

  /* Check for a blank win */
  if(!win) {
    /* Nothing to do */
    uttil_trace(UTTIL_TRACE_FUNC, "-uttil_gdk_get_window_pixmap: nothing\n");
    return 0;
  }

  /* Get the X windows pixmap */
  uttil_trace(UTTIL_TRACE_INFUNC, "  uttil_get_window_pixmap\n");
  p = uttil_get_window_pixmap(GDK_WINDOW_XDISPLAY(win),
			      GDK_WINDOW_XWINDOW(win));
  uttil_trace(UTTIL_TRACE_INFUNC, "  p=%p\n", p);

  /* Build a gdk pixmap from it */
  gp = gdk_pixmap_foreign_new(p);
  uttil_trace(UTTIL_TRACE_INFUNC, "  gp=%p\n", gp);

  /* Can't free this pixmap because of the gtk_pixmap */

  /* Return it */
  uttil_trace(UTTIL_TRACE_FUNC, "-uttil_gdk_get_window_pixmap: success\n");
  return gp;
}

/*
** uttil_gdk_get_window_desktop_pixmap
*/

GdkPixmap *uttil_gdk_get_window_desktop_pixmap(GdkWindow *win)
{
  /* Variables */
  GdkWindow *rw;
  GdkPixmap *rp, *wp;
  GdkGC *gc;
  int px, py, pw, ph, pb, pd;
  int wx, wy, ww, wh, wb, wd;

  /* Make noise */
  uttil_trace(UTTIL_TRACE_FUNC, "+%s: win=%p\n",
	      "uttil_gdk_get_window_desktop_pixmap", win);

  /* Get the desktop win */
  rw = uttil_gdk_get_window_desktop(win);
  if(!rw) {
    /* No desktop win */
    uttil_trace(UTTIL_TRACE_NOTICE,
		"%s: Cannot get desktop win from win (%p).\n",
		"uttil_gdk_get_window_desktop_pixmap",
		win);
    uttil_trace(UTTIL_TRACE_FUNC,
		"-uttil_gdk_get_window_desktop_pixmap: no root window\n");
    return 0;
  }

  /* Get the desktop pixmap */
  rp = uttil_gdk_get_window_pixmap(rw);
  if(!rp) {
    /* Get get pixmap from */
    uttil_trace(UTTIL_TRACE_NOTICE,
		"%s: Cannot get pixmap from win (%p).\n",
		"uttil_gdk_get_window_desktop_pixmap",
		rw);
    uttil_trace(UTTIL_TRACE_FUNC,
		"-uttil_gdk_get_window_desktop_pixmap: no root desktop\n");
    return 0;
  }

  /* Get the size of the pixmap and window to be put in */
  gdk_window_get_geometry(rp, 0, 0, &pw, &ph, 0);
  gdk_window_get_geometry(win, 0, 0, &ww, &wh, &wd);
  gdk_window_get_deskrelative_origin(win, &wx, &wy);

  /* Create a new pixmap */
  gc = gdk_gc_new(win);
  wp = gdk_pixmap_new(win, ww, wh, wd);

  /* Put the root window's pixmap into the new one */
  uttil_trace(UTTIL_TRACE_INFUNC,
	      "  gdk_draw_pixmap: wp=%p, gc=%p\n", wp, gc);
  gdk_draw_pixmap(wp, gc, rp,
		  wx, wy, 0, 0, ww, wh);

  /* Free things */
  gdk_gc_destroy(gc);
  gdk_imlib_free_pixmap(rp);
  /*  gdk_pixmap_unref(rp);*/

  /* Return it */
  uttil_trace(UTTIL_TRACE_FUNC,
	      "-uttil_gdk_get_window_desktop_pixmap: success\n");
  return wp;
}

/*
** uttil_gdk_set_window_desktop_pixmap
*/

int uttil_gdk_set_window_desktop_pixmap(GdkWindow *win)
{
  /* Variables */
  GdkPixmap *wp;

  /* Get the win pixmap */
  wp = uttil_gdk_get_window_desktop_pixmap(win);
  if(!wp) {
    /* Can't get it */
    uttil_trace(UTTIL_TRACE_NOTICE,
		"%s: Cannot get pixmap of the desktop of the win (%p).\n",
		"uttil_gdk_set_window_desktop_pixmap",
		win);
    return 1;
  }

  /* Set the background */
  gdk_window_set_back_pixmap(win, wp, 0);

  /* Free the pixmap */
  gdk_imlib_free_pixmap(wp);

  /* Return no problems */
  return 0;
}
